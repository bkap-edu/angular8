import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account/account.component';
import { SideBarComponent } from './side-bar/side-bar.component';



@NgModule({
  declarations: [AccountComponent, SideBarComponent],
  imports: [
    CommonModule
  ],
  exports: [AccountComponent]
})
export class AdminModule { }
