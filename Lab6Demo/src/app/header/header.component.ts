import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'header-area',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit{

  isCollapsed = true;

  constructor(){}

  ngOnInit(): void {
    
  }

  toggleCollapsed() {
    this.isCollapsed = !this.isCollapsed;
    console.log(this.isCollapsed);
  }  

}