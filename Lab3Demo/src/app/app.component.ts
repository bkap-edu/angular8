import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login/login.service';
import { User } from './_models/user';
import { Role } from './_models/role';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Lab3Demo';
  currentUser : User;

  constructor(
    private router: Router,
    private loginService: LoginService
  ){
    this.loginService.currentUser
  }
  

  get isAdmin() {
    return this.currentUser && this.currentUser.role === Role.Admin;
  }

  logout() {
      this.loginService.logout();
      this.router.navigate(['/login']);
  }
}
