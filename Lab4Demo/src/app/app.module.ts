import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TasksComponent } from './tasks/tasks.component';
import { TaskIconsComponent } from './task-icons/task-icons.component';
import { TaskTooltipDirective } from './task-tooltip.directive';
import { TaskEditorComponent } from './task-editor/task-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    TaskIconsComponent,
    TaskTooltipDirective,
    TaskEditorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
