import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../models/product.model';

const httpOptions={
  headers: new HttpHeaders({'Content-Type': 'aplication/json'})
};

const apiUrl = 'http://localhost:3000';

@Injectable({
  // providedIn: 'ProductComponent'
  providedIn:'root'
})
export class ApiService {

  constructor(private httpClient:HttpClient) { }

  getProducts():Observable<Product[]>{
    return this.httpClient.get<Product[]>(`${apiUrl}/products`);
  }

  getOne(id: number):Observable<Product>{
      return this.httpClient.get(`${apiUrl}/products/${id}`);
  }
}
