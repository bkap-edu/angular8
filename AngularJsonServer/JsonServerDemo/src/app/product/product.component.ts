import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { NgbdSortableHeader } from '../common/sortable.directive';
import { Product } from '../model/product.model';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
  providers:[ApiService]
})
export class ProductComponent implements OnInit {

  products:Product[]=[];
  total:number;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(private apiService:ApiService) { }

  ngOnInit(): void {
     this.apiService.getProducts().subscribe((res:Product[]) => {
       this.products = res;
       console.log(res);
     });
  }

}
