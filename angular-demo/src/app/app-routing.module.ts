import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountFormComponent } from './account-form/account-form.component';
import { AccountComponent } from './account/account.component';

const routes: Routes = [
  {path:"accounts", component:AccountComponent},
  {path:"accounts/register", component:AccountFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
