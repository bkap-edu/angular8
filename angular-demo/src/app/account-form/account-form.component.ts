import { Component, OnInit } from '@angular/core';
import { Account } from '../account.model';
import { AccountService } from '../account.service';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent implements OnInit {

  account:Account;

  constructor(private accountService:AccountService) {
      this.account = new Account();
      // this.account.id=2;
      // this.account.firstname="ssssss";
   }

  ngOnInit(): void {

  }


  createAccount(){
    //
    this.accountService.add(this.account).subscribe((res: any) => {
      console.log(res);
    });
  }
}
