import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Account } from './account.model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = 'http://localhost:3000/accounts';
@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Account[]> {
    return this.http.get<Account[]>(apiUrl)
      .pipe(
        tap(o => console.log('fetched Account')),
        catchError(this.handleError('getAccount', []))
      );
  }

  getOne(id: number): Observable<Account> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Account>(url).pipe(
      tap(_ => console.log(`fetched post id=${id}`)),
      catchError(this.handleError<Account>(`getPost id=${id}`))
    );
  }

  add(obj: Account): Observable<Account> {
    // console.log(obj);
    return this.http.post<Account>(apiUrl, obj , httpOptions);
  }

  update(id: any, post: Account): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, post, httpOptions).pipe(
      tap(_ => console.log(`updated account id=${id}`)),
      catchError(this.handleError<any>('update account'))
    );
  }

  delete(id: any): Observable<Account> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Account>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted account id=${id}`)),
      catchError(this.handleError<Account>('delete account'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
