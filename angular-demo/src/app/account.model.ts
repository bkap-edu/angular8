export class Account {
  id: number;
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
}
