import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CagtalogFormComponent } from './pages/cagtalog-form/cagtalog-form.component';
import { CatalogComponent } from './pages/catalog/catalog.component';
import { ProductFormComponent } from './pages/product-form/product-form.component';
import { ProductComponent } from './pages/product/product.component';

const routes: Routes = [
  {path: 'catalog', component: CatalogComponent},
  {path: 'catalog-add', component: CagtalogFormComponent},
  {path: 'product', component: ProductComponent},
  {path: 'product-add', component: ProductFormComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
