import { DecimalPipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from 'src/app/common/country';
import { NgbdSortableHeader, SortEvent } from 'src/app/common/sortable.directive';
import { Catalog } from 'src/app/models/catalog.model';
import { CatalogService } from 'src/app/services/catalog.service';
import { CountryService } from 'src/app/services/country.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css'],
  providers: [CatalogService, DecimalPipe]
})
export class CatalogComponent implements OnInit {

  catalogs$: Observable<Catalog[]>;
  total$: Observable<number>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public service: CatalogService) {
    this.catalogs$ = service.catalogs$;
    console.log(this.catalogs$);
    this.total$ = service.total$;
  }

  onSort({column, direction}: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  ngOnInit(): void {
  }

}
