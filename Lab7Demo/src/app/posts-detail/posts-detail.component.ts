import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Posts } from '../posts.model';

@Component({
  selector: 'app-posts-detail',
  templateUrl: './posts-detail.component.html',
  styleUrls: ['./posts-detail.component.scss']
})
export class PostsDetailComponent implements OnInit {

  obj: Posts = { id: 0, title: '', body: '' };
  isLoadingResults = true;

  constructor(private route: ActivatedRoute, private api: ApiService, private router: Router) { }

  ngOnInit() {
    this.getProductDetails(this.route.snapshot.params['id']);
  }

  getProductDetails(id: any) {
    this.api.getOne(id)
      .subscribe((data: any) => {
        this.obj = data;
        console.log(this.obj);
        this.isLoadingResults = false;
      });
  }

  delete(id: any) {
    this.isLoadingResults = true;
    this.api.delete(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.router.navigate(['/posts']);
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

}
