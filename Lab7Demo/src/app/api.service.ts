import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Posts } from './posts.model';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = 'https://jsonplaceholder.typicode.com/posts';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Posts[]> {
    return this.http.get<Posts[]>(apiUrl)
      .pipe(
        tap(posts => console.log('fetched posts')),
        catchError(this.handleError('getPosts', []))
      );
  }

  getOne(id: number): Observable<Posts> {
    const url = `${apiUrl}/${id}`;
    return this.http.get<Posts>(url).pipe(
      tap(_ => console.log(`fetched post id=${id}`)),
      catchError(this.handleError<Posts>(`getPost id=${id}`))
    );
  }

  add(post: Posts): Observable<Posts> {
    return this.http.post<Posts>(apiUrl, post, httpOptions).pipe(
      tap((posts: any) => console.log(`added post w/ id=${posts.id}`)),
      catchError(this.handleError<Posts>('addPost'))
    );
  }

  update(id: any, post: Posts): Observable<any> {
    const url = `${apiUrl}/${id}`;
    return this.http.put(url, post, httpOptions).pipe(
      tap(_ => console.log(`updated post id=${id}`)),
      catchError(this.handleError<any>('updatePost'))
    );
  }

  delete(id: any): Observable<Posts> {
    const url = `${apiUrl}/${id}`;
    return this.http.delete<Posts>(url, httpOptions).pipe(
      tap(_ => console.log(`deleted post id=${id}`)),
      catchError(this.handleError<Posts>('deletePost'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
