import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-posts-edit',
  templateUrl: './posts-edit.component.html',
  styleUrls: ['./posts-edit.component.scss']
})
export class PostsEditComponent implements OnInit {

  formGroup: FormGroup;
  id = '';
  prod_name = '';
  prod_desc = '';
  prod_price: number = null;
  isLoadingResults = false;
  matcher = new MyErrorStateMatcher();

  constructor(private router: Router, private route: ActivatedRoute, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.getOne(this.route.snapshot.params['id']);
    this.formGroup = this.formBuilder.group({
      'title' : [null, Validators.required],
      'body' : [null, Validators.required]
    });
  }

  getOne(id: any) {
    this.api.getOne(id).subscribe((data: any) => {
      this.id = data.id;
      this.formGroup.setValue({
        title: data.title,
        body: data.body,
      });
    });
  }

  onFormSubmit() {
    this.isLoadingResults = true;
    this.api.update(this.id, this.formGroup.value)
      .subscribe((res: any) => {
          const id = res.id;
          this.isLoadingResults = false;
          this.router.navigate(['/posts-details', id]);
        }, (err: any) => {
          console.log(err);
          this.isLoadingResults = false;
        }
      );
  }

  details() {
    this.router.navigate(['/posts-details', this.id]);
  }

}
