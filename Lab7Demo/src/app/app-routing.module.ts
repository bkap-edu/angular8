import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostsDetailComponent } from './posts-detail/posts-detail.component';
import { PostsAddComponent } from './posts-add/posts-add.component';
import { PostsEditComponent } from './posts-edit/posts-edit.component';


const routes: Routes = [
  {
    path: 'posts',
    component: PostsComponent,
    data: { title: 'List of posts' }
  },
  {
    path: 'posts-details/:id',
    component: PostsDetailComponent,
    data: { title: 'posts Details' }
  },
  {
    path: 'posts-add',
    component: PostsAddComponent,
    data: { title: 'Add posts' }
  },
  {
    path: 'posts-edit/:id',
    component: PostsEditComponent,
    data: { title: 'Edit posts' }
  },
  { path: '',
    redirectTo: '/posts',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
