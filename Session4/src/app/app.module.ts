import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from './component-parent-child/parent.component';
import { ChildComponent } from './component-parent-child/child.component';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { HeroChildComponentComponent } from './component-interaction/hero-child-component/hero-child-component.component';
import { HeroParentComponentComponent } from './component-interaction/hero-parent-component/hero-parent-component.component';
import { NameChildComponent } from './component-interaction/name-child/name-child.component';
import { NameParentComponent } from './component-interaction/name-parent/name-parent.component';
import { VoterComponent } from './component-interaction/voter.component';
import { VoteTakerComponent } from './component-interaction/votetaker.component';
import { CountdownLocalVarParentComponent } from './component-interaction/countdown-parent.component';
import { CountdownTimerComponent } from './component-interaction/countdown-timer.component';

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    ChildComponent,
    ComponentInteractionComponent,
    HeroChildComponentComponent,
    HeroParentComponentComponent,
    NameChildComponent,
    NameParentComponent,
    VoterComponent,
    VoteTakerComponent,
    CountdownTimerComponent,
    CountdownLocalVarParentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
