import { Component } from "@angular/core";

@Component({
  selector: 'app-child',
  template: `<p>parent child demo</p>`
})
export class ChildComponent{
  doSomething(){
    console.log('Childe called');
  }
}
