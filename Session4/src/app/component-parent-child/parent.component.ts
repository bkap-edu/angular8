// tslint:disable-next-line:quotemark
import { AfterViewInit, Component, ViewChild } from "@angular/core";
import { ChildComponent } from './child.component';

@Component({
  selector: 'app-parent',
  template: `<app-child></app-child>`,
})
export class ParentComponent implements AfterViewInit{
  @ViewChild(ChildComponent ) child: ChildComponent;

  ngAfterViewInit(){
    this.child.doSomething();
  }

}
