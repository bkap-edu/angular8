import { Component, Input, OnInit } from '@angular/core';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero-child',
  templateUrl: './hero-child-component.component.html',
})
export class HeroChildComponentComponent implements OnInit {

  @Input() hero: Hero;
  @Input('master') masterName:string;

  constructor() { }

  ngOnInit(): void {
  }

}
