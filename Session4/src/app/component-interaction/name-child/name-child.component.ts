import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-name-child',
  template: '<h3>{{name}}</h3>',
})
export class NameChildComponent implements OnInit {

  @Input()
  get name(): string {return this._name}
  set name(name:string){
    this._name=(name && name.trim()) || '<no name set>';
  }

  constructor() { }

  ngOnInit(): void {
  }

  private _name:string='';
}
