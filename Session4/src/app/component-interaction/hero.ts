export interface Hero {
  name: string;
}

export const HEROES = [
  {name: 'Trương Phi'},
  {name: 'Quan Vũ'},
  {name: 'Triệu Tử Long'}
];
