import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentInteractionComponent } from './component-interaction/component-interaction.component';
import { CountdownLocalVarParentComponent } from './component-interaction/countdown-parent.component';
import { NameParentComponent } from './component-interaction/name-parent/name-parent.component';
import { VoteTakerComponent } from './component-interaction/votetaker.component';
import { TextInterpolationComponent } from './databinding/text-interpolation.component';

const routes: Routes = [
  {path:'component-interaction', component:ComponentInteractionComponent},
  {path:'name-parent', component:NameParentComponent},
  {path:'vote-taker', component:VoteTakerComponent},
  {path:'count-down', component:CountdownLocalVarParentComponent},
  {path:'text', component:TextInterpolationComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
